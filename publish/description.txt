Gautrain commuters.

Tired of just missing the last train or raced to the train station just to find the next train is only departing in 30 minutes? Select your line and station and this app will countdown the departure of the next train or the train after that. In addition, it will also indicate whether its a 4 or 8 carriage - high, medium or low tariff - train. 

Disclaimer - The developer has no affiliation to Bombela or Gautrain. Based on the official schedule published by Gautrain. From time-to-time unforeseen circumstances result in trains not following schedule as indicated.