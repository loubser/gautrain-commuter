/*
 * nexTTrain
 * ---------
 *
 * Author: Loubser Kotzé
 * Date: 21 August 2017
 */
#pragma once

#include <pebble.h>

//! Number of train departures south - weekdays
#define DEPARTURE_SLOTS_SOUTH_WEEKDAY 64

//! Number of train departures South - weekends
#define DEPARTURE_SLOTS_SOUTH_WEEKEND 31

//! Number of departures slots north - weekdays
#define DEPARTURE_SLOTS_NORTH_WEEKDAY 63

//! Number of departures North - weekends
#define DEPARTURE_SLOTS_NORTH_WEEKEND 31

//! Number of departures West - Mondays
#define DEPARTURE_SLOTS_WEST_MONDAY 71

//! Number of departures West - Other
#define DEPARTURE_SLOTS_WEST_OTHER 66

//! Number of departures West - Saturday
#define DEPARTURE_SLOTS_WEST_SATURDAY 34

//! Number of departures West - Sunday
#define DEPARTURE_SLOTS_WEST_SUNDAY 40

//! Number of departures East - Mondays
#define DEPARTURE_SLOTS_EAST_MONDAY 73

//! Number of departures East - Other
#define DEPARTURE_SLOTS_EAST_OTHER 67

//! Number of departures East - Saturday
#define DEPARTURE_SLOTS_EAST_SATURDAY 35

//! Number of departures East - Sunday
#define DEPARTURE_SLOTS_EAST_SUNDAY 41

//! Max number of departures slots
#define MAX_NUMBER_OF_DEPARTURE_SLOTS 73

//! Number of stations on the North line
#define NUMBER_OF_STATIONS_NORTH 7

//! Number of stations on the South line
#define NUMBER_OF_STATIONS_SOUTH 7

//! Number of stations on the East line
#define NUMBER_OF_STATIONS_EAST 3

//! Number of stations on the West line
#define NUMBER_OF_STATIONS_WEST 3

//! Max number of stations
#define MAX_NUMBER_OF_STATIONS 7

//! Number of train lines North/South/East/West
#define NUMBER_OF_TRAIN_LINES 4

extern const uint8_t NO_MORE_DEPARTURES_TODAY;
extern const uint32_t TRAIN_SELECTION_PERSIST_KEY;

extern uint8_t TRAIN_SIZE_NORTH[DEPARTURE_SLOTS_NORTH_WEEKDAY];
extern uint8_t TRAIN_RATE_NORTH[DEPARTURE_SLOTS_NORTH_WEEKDAY][NUMBER_OF_STATIONS_NORTH];
extern uint8_t TRAIN_SIZE_SOUTH[DEPARTURE_SLOTS_SOUTH_WEEKDAY];
extern uint8_t TRAIN_RATE_SOUTH[DEPARTURE_SLOTS_SOUTH_WEEKDAY][NUMBER_OF_STATIONS_SOUTH];

extern uint8_t TRAIN_RATE_WEST_MONDAY[DEPARTURE_SLOTS_WEST_MONDAY][NUMBER_OF_STATIONS_WEST];
extern uint8_t TRAIN_RATE_WEST_OTHER[DEPARTURE_SLOTS_WEST_OTHER][NUMBER_OF_STATIONS_WEST];
extern uint8_t TRAIN_RATE_WEST_SATURDAY[DEPARTURE_SLOTS_WEST_SATURDAY][NUMBER_OF_STATIONS_WEST];
extern uint8_t TRAIN_RATE_WEST_SUNDAY[DEPARTURE_SLOTS_WEST_SUNDAY][NUMBER_OF_STATIONS_WEST];
extern uint8_t TRAIN_RATE_EAST_MONDAY[DEPARTURE_SLOTS_EAST_MONDAY][NUMBER_OF_STATIONS_EAST];
extern uint8_t TRAIN_RATE_EAST_OTHER[DEPARTURE_SLOTS_EAST_OTHER][NUMBER_OF_STATIONS_EAST];
extern uint8_t TRAIN_RATE_EAST_SATURDAY[DEPARTURE_SLOTS_EAST_SATURDAY][NUMBER_OF_STATIONS_EAST];
extern uint8_t TRAIN_RATE_EAST_SUNDAY[DEPARTURE_SLOTS_EAST_SUNDAY][NUMBER_OF_STATIONS_EAST];

// *****************************************************************************
// The offset in minutes from midnight used as the starting point to calculate
// the departure time of trains at stations on the specified line
// *****************************************************************************

extern uint16_t START_TIME_WEEKDAY[NUMBER_OF_TRAIN_LINES];
extern uint16_t START_TIME_WEEKEND[NUMBER_OF_TRAIN_LINES];

// *****************************************************************************
// Departure offset in minutes for each train on the various lines
// *****************************************************************************

extern uint16_t DEPARTURE_OFFSET_NORTH_WEEKDAY[DEPARTURE_SLOTS_NORTH_WEEKDAY];
extern uint16_t DEPARTURE_OFFSET_NORTH_WEEKEND[DEPARTURE_SLOTS_NORTH_WEEKEND];
extern uint16_t DEPARTURE_OFFSET_SOUTH_WEEKDAY[DEPARTURE_SLOTS_SOUTH_WEEKDAY];
extern uint16_t DEPARTURE_OFFSET_SOUTH_WEEKEND[DEPARTURE_SLOTS_SOUTH_WEEKEND];
extern uint16_t DEPARTURE_OFFSET_WEST_MONDAY[DEPARTURE_SLOTS_WEST_MONDAY];
extern uint16_t DEPARTURE_OFFSET_WEST_OTHER[DEPARTURE_SLOTS_WEST_OTHER];
extern uint16_t DEPARTURE_OFFSET_WEST_SATURDAY[DEPARTURE_SLOTS_WEST_SATURDAY];
extern uint16_t DEPARTURE_OFFSET_WEST_SUNDAY[DEPARTURE_SLOTS_WEST_SUNDAY];
extern uint16_t DEPARTURE_OFFSET_EAST_MONDAY[DEPARTURE_SLOTS_EAST_MONDAY];
extern uint16_t DEPARTURE_OFFSET_EAST_OTHER[DEPARTURE_SLOTS_EAST_OTHER];
extern uint16_t DEPARTURE_OFFSET_EAST_SATURDAY[DEPARTURE_SLOTS_EAST_SATURDAY];
extern uint16_t DEPARTURE_OFFSET_EAST_SUNDAY[DEPARTURE_SLOTS_EAST_SUNDAY];

// *****************************************************************************
// The station offset in minutes for each train per line
// *****************************************************************************

extern uint8_t STATION_OFFSET[NUMBER_OF_TRAIN_LINES][MAX_NUMBER_OF_STATIONS];