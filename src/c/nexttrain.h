/*
 * nexTTrain
 * ---------
 * 
 * Author:  Loubser Kotzé
 * Date:    8 August 2017
 */
#pragma once

#include <pebble.h>

typedef enum {
    S_HATFIELD = 0,
    S_PRETORIA = 1,
    S_CENTURION = 2,
    S_MIDRAND = 3,
    S_MARLBORO = 4,
    S_SANDTON = 5,
    S_ROSEBANK = 6
} TrainStationSouth;

typedef enum {
    N_PARK = 0,
    N_ROSEBANK = 1,
    N_SANDTON = 2,
    N_MARLBORO = 3,
    N_MIDRAND = 4,
    N_CENTURION = 5,
    N_PRETORIA = 6
} TrainStationNorth;

typedef enum {
    W_TAMBO = 0,
    W_RHODESFIELD = 1,
    W_MARLBORO = 2
} TrainStationWest;

typedef enum {
    E_SANDTON = 0,
    E_MARLBORO = 1,
    E_RHODESFIELD = 2
} TrainStationEast;

typedef enum {
    LINE_SOUTH,
    LINE_NORTH,
    LINE_EAST,
    LINE_WEST
} TrainLine;

typedef enum {
    SUN,
    MON,
    TUE,
    WED,
    THU,
    FRI,
    SAT
} DayOfTheWeek;

typedef enum {
    LOW = 0,
    MEDIUM = 1,
    HIGH = 2,
    NO_SERVICE = 3
} DepartureRate;

typedef enum {
    SHORT = 0,
    LONG = 1
} TrainSize;

typedef struct DepartureSlot {
    uint8_t slot_index;
    uint16_t departure_minute;
    TrainSize train_size;
    DepartureRate departure_rate;
} DepartureSlot;

typedef struct TrainLineStation {
    TrainLine line;
    uint8_t station;
} TrainLineStation;

typedef struct WindowContext {
    struct TrainLineStation *train_line_station;
    struct tm *tick_time;
    TrainLine temp_selected_line;
} WindowContext;
