
/*
 * nexTTrain
 * ---------
 * 
 * Author: Loubser Kotze
 * Date: 17 August 2017
 */
#pragma once

void trace(char *trace_message, void *context);

void printDepartureSlot(DepartureSlot *departure_slot);

struct tm *current_time();

void timecpy(struct tm *destination, struct tm *source);

/**
 * @param time_string Returns a formatted string, hh:mm, for the specified time
 * @param time_in_minutes The time in minutes
 */
void parse_time(char *time_string, int size, uint16_t time_in_minutes, TimeUnits time_unit);

int32_t diff_in_seconds(struct tm *from_time, uint16_t to_time_in_mintues);

bool is_color_pebble();

bool is_round_pebble();
