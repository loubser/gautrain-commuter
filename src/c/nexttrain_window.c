/*
 * nexTTrain
 * ---------
 * 
 * Author:  Loubser Kotzé
 * Date:    8 August 2017
 */
#include "nexttrain.h"
#include "utility.h"

static Window *s_window;
static MenuLayer *s_menu_layer;
static TextLayer *s_text_layer;

static GFont TITLE_FONT;
static GFont SUBTITLE_FONT;

static const uint16_t BADGE_INSET = 1;
static const uint16_t BADGE_Y_OFFSET = -9;
static const uint16_t BADGE_RADIUS = 11;
static const uint16_t ICON_INSET = 0;
static const uint16_t TITLE_Y_OFFSET = 0;
static const uint16_t SUBTITLE_Y_OFFSET = 23;

#define LINE_SELECTION_CELL_HEIGHT 56
#define LINE_SELECTION_CELL_HEIGHT_ROUND 60

extern struct WindowContext *window_context_create();
extern void window_context_destroy(struct WindowContext *window_context);

extern void resources_initialise();
extern void resources_destroy();

extern GBitmap *get_train_icon(bool inverse);
extern GBitmap *get_track_icon(bool inverse);

extern char *description_train_line(TrainLine train_line);
extern char *description_train_station(TrainLine train_line, uint8_t train_station);

extern uint8_t number_of_departure_slots(TrainLine train_line, DayOfTheWeek day);

extern void set_departure_slot_details_for_time(DepartureSlot *departure_slot, TrainLineStation *train_line_station, struct tm *for_time);
extern void set_departure_slot_details_for_index(DepartureSlot *departure_slot, TrainLineStation *train_line_station, DayOfTheWeek departure_day, uint8_t slot_index);

extern void train_line_selection_window_push(WindowContext *context);

static void text_layer_update(WindowContext *context);

static void update_arrival_time_data(struct tm *tick_time, TimeUnits unit_changed) {

    trace("update_arival_time_data", window_get_user_data(s_window));

    WindowContext *context = (WindowContext *) window_get_user_data(s_window);
    timecpy(context->tick_time, current_time());

    text_layer_update(context);

    layer_mark_dirty(menu_layer_get_layer(s_menu_layer));
    layer_mark_dirty(text_layer_get_layer(s_text_layer));

}

// ---------------------------------------------------------------------------------------------------------------------
// Menu layer  & call back functions
// ---------------------------------------------------------------------------------------------------------------------

static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *callback_context) {

    trace("get_num_rows_callback", callback_context);

    WindowContext *context = (WindowContext *) callback_context;
    return number_of_departure_slots(context->train_line_station->line, context->tick_time->tm_wday);

}

static void draw_train_badge(GContext *ctx, const Layer *layer, DepartureSlot *departure_slot, uint16_t b_center_x) {

    GSize l_size = layer_get_bounds(layer).size;
    GPoint badge_centre = GPoint(b_center_x - BADGE_INSET, l_size.h / 2 + BADGE_Y_OFFSET);

    GColor rate_color;
    GColor rate_border;
    GColor text_color = GColorBlack;

    if (is_color_pebble()) {
        if (departure_slot->departure_rate == LOW) {
            // draw a green badge
            rate_color = GColorJaegerGreen;//GColorMintGreen;
            rate_border = GColorBlack;
        } else if (departure_slot->departure_rate == MEDIUM) {
            // draw a yellow badge
            rate_color = GColorLimerick;//GColorPastelYellow;
            rate_border = GColorBlack;
        } else {
            // draw a red badge
            rate_color = GColorMelon;
            rate_border = GColorDarkCandyAppleRed;
        }
    } else {
        rate_color = GColorWhite;
        rate_border = GColorBlack;
    }

    graphics_context_set_fill_color(ctx, rate_color);
    graphics_context_set_stroke_color(ctx, rate_border);
    graphics_context_set_stroke_width(ctx, 1);
    graphics_context_set_antialiased(ctx, true);
    GRect badge_bounds = GRect(badge_centre.x - BADGE_RADIUS, badge_centre.y - BADGE_RADIUS, BADGE_RADIUS * 2, BADGE_RADIUS * 2);

    switch (departure_slot->departure_rate) {
        case LOW:
            // draw a round badge
            graphics_fill_circle(ctx, badge_centre, BADGE_RADIUS);
            graphics_draw_circle(ctx, badge_centre, BADGE_RADIUS);
            break;
        case MEDIUM:
            // draw a square badge with round corners
            graphics_fill_rect(ctx, badge_bounds, 7, GCornersAll);
            graphics_draw_round_rect(ctx, badge_bounds, 7);
            break;
        case HIGH:
            // draw a square badge
            graphics_fill_rect(ctx, badge_bounds, 0, GCornersAll);
            graphics_draw_round_rect(ctx, badge_bounds, 0);
            break;
        case NO_SERVICE:
            break;
    }

    // draw train size number inside badge
    graphics_context_set_text_color(ctx, text_color);
    GFont font = fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD);
    char *size = (departure_slot->train_size == SHORT) ? "4" : "8";
    GRect font_bounds = GRect(badge_centre.x - BADGE_RADIUS, badge_centre.y - BADGE_RADIUS - 6, BADGE_RADIUS * 2, BADGE_RADIUS * 2);
    graphics_draw_text(ctx, size, font, font_bounds, GTextOverflowModeFill, GTextAlignmentCenter, NULL);

}

static void draw_menu_item(GContext *ctx, const Layer *layer, GBitmap *icon, char *title, char *subtitle, DepartureSlot *departure_slot, uint8_t inset) {

    //! @todo work out the coordinates for each item only once and save into global variables to improve performance and reduce number of calculations on each screen update
    
    GSize layer_size = layer_get_bounds(layer).size;
    GSize icon_size = gbitmap_get_bounds(icon).size;
    GSize title_size = graphics_text_layout_get_content_size(title, TITLE_FONT, GRect(0, 0, layer_size.w, layer_size.h), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter);
    GSize subtitle_size = graphics_text_layout_get_content_size(subtitle, SUBTITLE_FONT, GRect(0, 0, layer_size.w, layer_size.h), GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter);

    // required width
    uint16_t total_width = inset + icon_size.w + title_size.w + BADGE_RADIUS + BADGE_RADIUS + inset;

    // scaling factor to fit on menu layer
    uint scale = layer_size.w / total_width;

    // icon center & position
    uint16_t i_half_width = icon_size.w / 2;
    uint16_t i_center_x = (inset + icon_size.w / 2) * layer_size.w / total_width;
    GRect i_pos = GRect(i_center_x - i_half_width, layer_size.h / 2 - icon_size.h / 2, icon_size.w, icon_size.h);
        
    // title center & position
    uint16_t title_half_width = title_size.w / 2;
    uint16_t title_center_x = (inset + icon_size.w + title_size.w / 2) * layer_size.w / total_width - 3;
    GRect title_pos = GRect(title_center_x - title_half_width, TITLE_Y_OFFSET, title_size.w, title_size.h);
    
    // subtitle center & position
    uint16_t subtitle_half_width = subtitle_size.w / 2;
    GRect subtitle_pos = GRect(title_center_x - subtitle_half_width, SUBTITLE_Y_OFFSET, subtitle_size.w, subtitle_size.h);

    // badge center & position
    uint16_t b_half_width = BADGE_RADIUS;
    uint16_t b_center_x = (inset + icon_size.w + title_size.w + b_half_width) * layer_size.w / total_width;

    // draw icon
    graphics_context_set_compositing_mode(ctx, GCompOpSet);
    graphics_draw_bitmap_in_rect(ctx, icon, i_pos);
    // draw title
    graphics_draw_text(ctx, title, TITLE_FONT, title_pos, GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
    // draw subtitle
    graphics_draw_text(ctx, subtitle, SUBTITLE_FONT, subtitle_pos, GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL);
    // draw badge
    draw_train_badge(ctx, layer, departure_slot, b_center_x);

}

static void draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context) {

    trace("draw_row_callback", callback_context);

    WindowContext *context = (WindowContext *) callback_context;

    struct DepartureSlot departure_slot;
    set_departure_slot_details_for_index(&departure_slot, context->train_line_station, context->tick_time->tm_wday, cell_index->row);

    int32_t seconds_to_departure = diff_in_seconds(context->tick_time, departure_slot.departure_minute);

    bool selected = menu_layer_is_index_selected(s_menu_layer, cell_index);
    static char s_title[10];
    static char s_subtitle[20];
    GBitmap *icon;

    if (seconds_to_departure > 0) {
        parse_time(s_title, sizeof (s_title), seconds_to_departure, SECOND_UNIT);
        icon = get_train_icon(selected);
    } else {
        strcpy(s_title, "00:00:00");
        icon = get_track_icon(selected);
    }
    parse_time(s_subtitle, sizeof (s_subtitle), departure_slot.departure_minute, MINUTE_UNIT);

    // menu_cell_basic_draw(ctx, cell_layer, s_title, s_subtitle, icon);
    draw_menu_item(ctx, cell_layer, icon, s_title, s_subtitle, &departure_slot, is_round_pebble() ? 7 : 2);

}

static int16_t get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {

    trace("get_cell_height_callback", callback_context);

    //    if (is_round_pebble()) {
    //
    //        return menu_layer_is_index_selected(menu_layer, cell_index)
    //                ? MENU_CELL_ROUND_UNFOCUSED_TALL_CELL_HEIGHT
    //                : MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT;
    //
    //    } else {

    return LINE_SELECTION_CELL_HEIGHT;

    //    }

}

static void get_drawbackground_callback(GContext* ctx, const Layer *bg_layer, bool highlight, void *callback_context) {

    //    APP_LOG(APP_LOG_LEVEL_DEBUG, "draw background");

}

static void menu_layer_init(Layer *parent_layer) {

    trace("menu_layer_init", NULL);

    GRect bounds = layer_get_bounds(parent_layer);
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Main window layer dimensions (%d,%d,%d,%d)", bounds.origin.x, bounds.origin.y, bounds.size.w, bounds.size.h);
    WindowContext *callback_context = window_get_user_data(s_window);

    s_menu_layer = menu_layer_create(GRect(bounds.origin.x, PBL_IF_ROUND_ELSE(LINE_SELECTION_CELL_HEIGHT_ROUND, LINE_SELECTION_CELL_HEIGHT), bounds.size.w, PBL_IF_ROUND_ELSE(LINE_SELECTION_CELL_HEIGHT_ROUND, LINE_SELECTION_CELL_HEIGHT) * 2));

    struct MenuLayerCallbacks menu_layer_callbacks = {
        .get_num_rows = get_num_rows_callback,
        .draw_row = draw_row_callback,
        .get_cell_height = get_cell_height_callback,
        .draw_background = get_drawbackground_callback
    };

    menu_layer_set_callbacks(s_menu_layer, callback_context, menu_layer_callbacks);
    menu_layer_pad_bottom_enable(s_menu_layer, false);
    menu_layer_set_center_focused(s_menu_layer, false);
    menu_layer_set_highlight_colors(s_menu_layer, COLOR_FALLBACK(GColorBabyBlueEyes, GColorBlack), COLOR_FALLBACK(GColorBlack, GColorWhite));

    layer_add_child(parent_layer, menu_layer_get_layer(s_menu_layer));

}

// ---------------------------------------------------------------------------------------------------------------------
// Text layer
// ---------------------------------------------------------------------------------------------------------------------

static void text_layer_update(WindowContext *context) {

    char *line_name = description_train_line(context->train_line_station->line);
    char *station_name = description_train_station(context->train_line_station->line, context->train_line_station->station);

    char print_time[10];
    static char selection_detail[50];

    strftime(print_time, 10, "%H:%M", context->tick_time);
    snprintf(selection_detail, 50, "%s\n%s:%s", print_time, station_name, line_name);
    text_layer_set_text(s_text_layer, selection_detail);

}

static void text_layer_init(Layer *parent_layer, WindowContext *context) {

    trace("text_layer_init", NULL);

    GRect bounds = layer_get_bounds(parent_layer);

    s_text_layer = text_layer_create(GRect(0, 0, bounds.size.w, LINE_SELECTION_CELL_HEIGHT));
    text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
    text_layer_set_overflow_mode(s_text_layer, GTextOverflowModeWordWrap);
    text_layer_set_font(s_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
    text_layer_update(context);

    layer_add_child(parent_layer, text_layer_get_layer(s_text_layer));

}

// ---------------------------------------------------------------------------------------------------------------------
// Window click handler functions
// ---------------------------------------------------------------------------------------------------------------------

static void select_click_handler(ClickRecognizerRef recognizer, void *callback_context) {

    trace("select_click_handler", callback_context);

    train_line_selection_window_push(callback_context);

}

static void up_click_handler(ClickRecognizerRef recognizer, void *callback_context) {

    trace("up_click_handler", callback_context);

    menu_layer_set_selected_next(s_menu_layer, true, MenuRowAlignTop, true);

}

static void down_click_handler(ClickRecognizerRef recognizer, void *callback_context) {

    trace("down_click_handler", callback_context);

    menu_layer_set_selected_next(s_menu_layer, false, MenuRowAlignTop, true);

}

static void click_config_provider(void *context) {

    window_single_click_subscribe(BUTTON_ID_SELECT, select_click_handler);
    window_single_click_subscribe(BUTTON_ID_UP, up_click_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN, down_click_handler);

}

// ---------------------------------------------------------------------------------------------------------------------
// Window handler functions
// ---------------------------------------------------------------------------------------------------------------------

static void window_load_handler(Window *window) {

    trace("window_load_handler", window_get_user_data(window));

    WindowContext *context = window_get_user_data(s_window);
    text_layer_init(window_get_root_layer(window), context);
    menu_layer_init(window_get_root_layer(window));
    resources_initialise();

}

static void window_unload_handler(Window *window) {

    trace("window_unload_handler", window_get_user_data(window));

    menu_layer_destroy(s_menu_layer);
    resources_destroy();
    window_context_destroy(window_get_user_data(s_window));

}

static void window_appear_handler(Window *window) {

    trace("window_appear_handler", window_get_user_data(window));

    tick_timer_service_subscribe(SECOND_UNIT, update_arrival_time_data);
    WindowContext *context = (WindowContext *) window_get_user_data(s_window);
    update_arrival_time_data(context->tick_time, SECOND_UNIT);

    text_layer_update(context);
    struct DepartureSlot departure_slot;
    set_departure_slot_details_for_time(&departure_slot, context->train_line_station, context->tick_time);
    menu_layer_set_selected_index(s_menu_layer, (MenuIndex){0, departure_slot.slot_index}, MenuRowAlignTop, true);

}

static void window_disappear_handler(Window *window) {

    trace("window_disappear_handler", window_get_user_data(window));

    tick_timer_service_unsubscribe();

}

// ---------------------------------------------------------------------------------------------------------------------
// Window create and destroy functions
// ---------------------------------------------------------------------------------------------------------------------

static void window_init(void) {

    struct WindowContext *context = window_context_create();

    s_window = window_create();
    struct WindowHandlers window_handlers = {
        .load = window_load_handler,
        .unload = window_unload_handler,
        .appear = window_appear_handler,
        .disappear = window_disappear_handler
    };
    window_set_user_data(s_window, context);
    window_set_window_handlers(s_window, window_handlers);
    window_set_click_config_provider_with_context(s_window, click_config_provider, context);

    // set reusable vars
    TITLE_FONT = fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD);
    SUBTITLE_FONT = fonts_get_system_font(FONT_KEY_GOTHIC_24);

}

static void window_deinit(void) {

    window_destroy(s_window);
    s_window = NULL;

}

void nexttrain_window_push() {

    if (!s_window) {
        window_init();
        APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", s_window);
    }
    window_stack_push(s_window, true);

}

int main(void) {

    nexttrain_window_push();
    app_event_loop();
    window_deinit();

}