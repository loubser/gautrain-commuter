/*
 * nexTTrain
 * ---------
 * 
 * Author: Loubser Kotzé
 * Date: 17 August 2017
 */
#include <pebble.h>

#include "nexttrain.h"
#include "utility.h"

extern char *description_train_line(TrainLine train_line);
extern char *description_train_station(TrainLine train_line, uint8_t train_station);

struct tm *current_time() {
    time_t tick_time = time(NULL);
    struct tm *p_current_time = localtime(&tick_time);

    // Modify to test app at different times
    /*
    p_current_time->tm_wday = MON;
    p_current_time->tm_mday = 9;
    p_current_time->tm_mon = 9;
    p_current_time->tm_year=2017;
    p_current_time->tm_hour = 6;
    p_current_time->tm_min = 15;
    */
    
    return p_current_time;
}

void parse_time(char *time_string, int size, uint16_t time_in_minutes_or_seconds, TimeUnits time_unit) {

    trace("parse_time", NULL);

    if (time_unit == MINUTE_UNIT) {
        uint16_t hour = time_in_minutes_or_seconds / 60;
        uint16_t minutes = time_in_minutes_or_seconds - hour * 60;
        snprintf(time_string, size, "%02d:%02d", hour, minutes);
    } else if (time_unit == SECOND_UNIT) {
        uint16_t hour = time_in_minutes_or_seconds / 3600;
        uint16_t minutes = (time_in_minutes_or_seconds - hour * 3600) / 60;
        uint16_t seconds = time_in_minutes_or_seconds - hour * 3600 - minutes * 60;
        snprintf(time_string, size, "%02d:%02d:%02d", hour, minutes, seconds);
    }

}

void timecpy(struct tm *destination, struct tm * source) {

    trace("timecpy", NULL);

    destination->tm_gmtoff = source->tm_gmtoff;
    destination->tm_hour = source->tm_hour;
    destination->tm_isdst = source->tm_isdst;
    destination->tm_mday = source->tm_mday;
    destination->tm_min = source->tm_min;
    destination->tm_mon = source->tm_mon;
    destination->tm_sec = source->tm_sec;
    destination->tm_wday = source->tm_wday;
    destination->tm_yday = source->tm_yday;
    destination->tm_year = source->tm_year;
    strcpy(destination->tm_zone, source->tm_zone);

}

int32_t diff_in_seconds(struct tm *from_time, uint16_t to_time_in_mintues) {

    trace("diff_in_seconds", NULL);

    uint32_t from_time_in_seconds = from_time->tm_hour * 3600 + from_time->tm_min * 60 + from_time->tm_sec;
    uint32_t to_time_in_seconds = to_time_in_mintues * 60;

    return to_time_in_seconds - from_time_in_seconds;

}

bool is_round_pebble() {

    return PBL_IF_ROUND_ELSE(true, false);

}

bool is_color_pebble() {

    return PBL_IF_COLOR_ELSE(true, false);

}

void trace(char *trace_message, void *context) {
    if (false) {
        if (context) {
            WindowContext *window_context = (WindowContext *) context;
            APP_LOG(APP_LOG_LEVEL_DEBUG, ">>> %s (%s,%s)", trace_message, description_train_line(window_context->train_line_station->line), description_train_station(window_context->train_line_station->line, window_context->train_line_station->station));
        } else {
            APP_LOG(APP_LOG_LEVEL_DEBUG, ">>> %s", trace_message);
        }
    }
}

void printDepartureSlot(DepartureSlot *departure_slot) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "slot_index:%d, departure_minute:%d, train_size:%d, departure_rate:%d", departure_slot->slot_index, departure_slot->departure_minute, departure_slot->train_size, departure_slot->departure_rate);
}
