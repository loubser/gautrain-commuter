/*
 * nexTTrain
 * ---------
 * 
 * Author:  Loubser Kotzé
 * Date:    8 August 2017
 */
#include <pebble.h>

#include "nexttrain.h"
#include "utility.h"
#include "train_data.h"

//******************************************************************************
//                               Resources
//******************************************************************************

GBitmap *icon_train_black;
GBitmap *icon_train_white;
GBitmap *icon_track_black;
GBitmap *icon_track_white;

/**
 * Allocate fixed memory to store window context which can be used by the 
 * various pebble window management functions. If possible, restore context 
 * state from previous session.
 */
struct WindowContext *window_context_create() {

    trace("window_context_create", NULL);

    struct WindowContext *window_context = malloc(sizeof (struct WindowContext));
    if (window_context == NULL) return NULL;

    window_context->train_line_station = malloc(sizeof (struct TrainLineStation));
    if (window_context->train_line_station == NULL) {
        free(window_context);
        return NULL;
    }

    window_context->tick_time = malloc(sizeof (struct tm));
    if (window_context->tick_time == NULL) {
        free(window_context->train_line_station);
        free(window_context);
        return NULL;
    }

    if (persist_exists(TRAIN_SELECTION_PERSIST_KEY)) {
        persist_read_data(TRAIN_SELECTION_PERSIST_KEY, window_context->train_line_station, sizeof (struct TrainLineStation));
    } else {
        window_context->train_line_station->line = LINE_SOUTH;
        window_context->train_line_station->station = S_HATFIELD;
    }
    timecpy(window_context->tick_time, current_time());
    window_context->temp_selected_line = window_context->train_line_station->line;

    return window_context;
}

/**
 * Free up the memory occupied by window_context
 */
void window_context_destroy(struct WindowContext *window_context) {

    trace("window_context_destroy", window_context);

    // First persist the context to storage
    persist_write_data(TRAIN_SELECTION_PERSIST_KEY, window_context->train_line_station, sizeof (struct TrainLineStation));

    if (window_context != NULL) {
        if (window_context->train_line_station != NULL) {
            free(window_context->train_line_station);
        }
        if (window_context->tick_time != NULL) {
            free(window_context->tick_time);
        }
        free(window_context);
    }

}

/**
 * Utility function to copy train selection data and time into the supplied 
 * window context structure
 */
void window_context_set(struct WindowContext *window_context, TrainLineStation *selection_data, tm * tick_time) {

    trace("window_context_set", NULL);

    window_context->train_line_station->line = selection_data->line;
    window_context->train_line_station->station = selection_data->station;
    timecpy(window_context->tick_time, tick_time);

}

/**
 * Initialize application resources
 */
void resources_initialise() {

    icon_train_black = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_TRAIN_BLACK);
    icon_track_black = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_TRACK_BLACK);
    icon_train_white = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_TRAIN_WHITE);
    icon_track_white = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_TRACK_WHITE);

}

/**
 * Destroy application resources
 */
void resources_destroy() {

    gbitmap_destroy(icon_train_black);
    gbitmap_destroy(icon_track_black);
    gbitmap_destroy(icon_train_white);
    gbitmap_destroy(icon_track_white);

}

/**
 * @return The appropriate train icon for the specified input parameters
 */
GBitmap *get_train_icon(bool inverse) {

    if (is_color_pebble()) {
        return icon_train_black;
    } else {
        return inverse ? icon_train_white : icon_train_black;
    }

}

/**
 * @return The appropriate track icon for the specified input parameters
 */
GBitmap *get_track_icon(bool inverse) {

    if (is_color_pebble()) {
        return icon_track_black;
    } else {
        return inverse ? icon_track_white : icon_track_black;
    }


}

/**
 * @return The description for the specified train line
 */
char *description_train_line(TrainLine train_line) {

    switch (train_line) {
        case LINE_SOUTH: return "South";
        case LINE_NORTH: return "North";
        case LINE_EAST: return "East";
        case LINE_WEST: return "West";
        default: return "";
    }

}

/**
 * @return The description for the specified train station
 */
char *description_train_station(TrainLine train_line, uint8_t train_station) {

    switch (train_line) {
        case LINE_NORTH:
            switch (train_station) {
                case N_PARK: return "Park";
                case N_ROSEBANK: return "Rosebank";
                case N_SANDTON: return "Sandton";
                case N_MARLBORO: return "Marlboro";
                case N_MIDRAND: return "Midrand";
                case N_CENTURION: return "Centurion";
                case N_PRETORIA: return "Pretoria";
                default: return "";
            };
        case LINE_SOUTH:
            switch (train_station) {
                case S_HATFIELD: return "Hatfield";
                case S_PRETORIA: return "Pretoria";
                case S_CENTURION: return "Centurion";
                case S_MIDRAND: return "Midrand";
                case S_MARLBORO: return "Marlboro";
                case S_SANDTON: return "Sandton";
                case S_ROSEBANK: return "Rosebank";
                default: return "";
            };
        case LINE_EAST:
            switch (train_station) {
                case E_SANDTON: return "Sandton";
                case E_MARLBORO: return "Marlboro";
                case E_RHODESFIELD: return "Rhodesfield";
                default: return "";
            };
        case LINE_WEST: switch (train_station) {
                case W_TAMBO: return "OR Tambo";
                case W_RHODESFIELD: return "Rhodesfield";
                case W_MARLBORO: return "Marlboro";
                default: return "";
            };
        default: return "";
    }

}

/**
 * @return The number of stations on the specified line
 */
uint8_t number_of_stations(TrainLine train_line) {

    switch (train_line) {
        case LINE_NORTH: return NUMBER_OF_STATIONS_NORTH;
        case LINE_SOUTH: return NUMBER_OF_STATIONS_SOUTH;
        case LINE_EAST: return NUMBER_OF_STATIONS_EAST;
        case LINE_WEST: return NUMBER_OF_STATIONS_WEST;
        default: return MAX_NUMBER_OF_STATIONS;
    }

}

/**
 * @return The number of departure slots on the specified line and day
 */
uint8_t number_of_departure_slots(TrainLine train_line, DayOfTheWeek day) {

    bool isWeekend = (day == SAT || day == SUN);
    switch (train_line) {
        case LINE_NORTH:
            return (isWeekend)
                    ? DEPARTURE_SLOTS_NORTH_WEEKEND
                    : DEPARTURE_SLOTS_NORTH_WEEKDAY;
        case LINE_SOUTH:
            return (isWeekend)
                    ? DEPARTURE_SLOTS_SOUTH_WEEKEND
                    : DEPARTURE_SLOTS_SOUTH_WEEKDAY;
        case LINE_WEST:
            switch (day) {
                case MON: return DEPARTURE_SLOTS_WEST_MONDAY;
                case SAT: return DEPARTURE_SLOTS_WEST_SATURDAY;
                case SUN: return DEPARTURE_SLOTS_WEST_SUNDAY;
                default: return DEPARTURE_SLOTS_WEST_OTHER;
            }
        case LINE_EAST:
            switch (day) {
                case MON: return DEPARTURE_SLOTS_EAST_MONDAY;
                case SAT: return DEPARTURE_SLOTS_EAST_SATURDAY;
                case SUN: return DEPARTURE_SLOTS_EAST_SUNDAY;
                default: return DEPARTURE_SLOTS_EAST_OTHER;
            }
        default:
            APP_LOG(APP_LOG_LEVEL_ERROR, "Number of departure slots not configured for train line %d, day of the week %d", train_line, day);
            return 0;
    }

}

/**
 * @return The train size for the specified line, departure slot and day
 */
TrainSize train_size(TrainLine train_line, uint8_t departure_slot, DayOfTheWeek day) {

    if (train_line == LINE_EAST || train_line == LINE_WEST || day == SAT || day == SUN) {
        return SHORT;
    } else {
        return (train_line == LINE_NORTH) ? TRAIN_SIZE_NORTH[departure_slot] : TRAIN_SIZE_SOUTH[departure_slot];
    }

}

/**
 * @return Departure time in minutes for the first train on the specified line and day
 */
uint16_t scheduled_start_time(TrainLine train_line, DayOfTheWeek day) {

    return (day == SAT || day == SUN)
            ? START_TIME_WEEKEND[train_line]
            : START_TIME_WEEKDAY[train_line];

}

/**
 * @return Departure time offset in minutes for the specified line, departure slot and day
 */
uint16_t departure_slot_offset(TrainLine train_line, uint8_t departure_slot, DayOfTheWeek day) {

    bool isWeekend = (day == SAT || day == SUN);
    switch (train_line) {
        case LINE_SOUTH:
            return (isWeekend)
                    ? DEPARTURE_OFFSET_SOUTH_WEEKEND[departure_slot]
                    : DEPARTURE_OFFSET_SOUTH_WEEKDAY[departure_slot];
        case LINE_NORTH:
            return (isWeekend)
                    ? DEPARTURE_OFFSET_NORTH_WEEKEND[departure_slot]
                    : DEPARTURE_OFFSET_NORTH_WEEKDAY[departure_slot];
        case LINE_EAST:
            switch (day) {
                case MON: return DEPARTURE_OFFSET_EAST_MONDAY[departure_slot];
                case SAT: return DEPARTURE_OFFSET_EAST_SATURDAY[departure_slot];
                case SUN: return DEPARTURE_OFFSET_EAST_SUNDAY[departure_slot];
                default: return DEPARTURE_OFFSET_EAST_OTHER[departure_slot];
            }
        case LINE_WEST:
            switch (day) {
                case MON: return DEPARTURE_OFFSET_WEST_MONDAY[departure_slot];
                case SAT: return DEPARTURE_OFFSET_WEST_SATURDAY[departure_slot];
                case SUN: return DEPARTURE_OFFSET_WEST_SUNDAY[departure_slot];
                default: return DEPARTURE_OFFSET_WEST_OTHER[departure_slot];
            }
        default:
            return 0;
    }

}

/**
 * @return The departure minute for the specified line, station, day and departure slot
 */
uint16_t departure_minute(TrainLineStation *train_line_station, DayOfTheWeek departure_day, uint8_t slot_index) {

    trace("departure_minute", NULL);

    uint16_t start_time = scheduled_start_time(train_line_station->line, departure_day);
    uint16_t departure_offset = departure_slot_offset(train_line_station->line, slot_index, departure_day);
    uint8_t station_offset = STATION_OFFSET[(uint8_t) train_line_station->line][train_line_station->station];
    return start_time + departure_offset + station_offset;

}

/**
 * @return The departure rate for the specified line, station, day and departure slot
 */
DepartureRate departure_rate(TrainLineStation *train_line_station, DayOfTheWeek day, uint8_t slot_index) {

    trace("departure_rate", NULL);

    if (day == SAT || day == SUN) {
        return MEDIUM;
    } else {
        switch (train_line_station->line) {
            case LINE_NORTH: return TRAIN_RATE_NORTH[slot_index][train_line_station->station];
            case LINE_SOUTH: return TRAIN_RATE_SOUTH[slot_index][train_line_station->station];
            case LINE_EAST:
                return (day == MON)
                        ? TRAIN_RATE_EAST_MONDAY[slot_index][train_line_station->station]
                        : TRAIN_RATE_EAST_OTHER[slot_index][train_line_station->station];
            case LINE_WEST:
                return (day == MON)
                        ? TRAIN_RATE_WEST_MONDAY[slot_index][train_line_station->station]
                        : TRAIN_RATE_WEST_OTHER[slot_index][train_line_station->station];
            default: return MEDIUM;
        }
    }

}

/**
 * Copy departure details into the supplied departure_slot for the specified line, station, day, departure minute and slot
 */
void set_departure_slot_details_with_minutes(DepartureSlot *departure_slot, TrainLineStation *train_line_station, DayOfTheWeek departure_day, uint16_t departure_minute, uint8_t slot_index) {

    trace("set_departure_slot_details_with_minutes", NULL);

    departure_slot->slot_index = slot_index;
    departure_slot->departure_minute = departure_minute;
    departure_slot->train_size = train_size(train_line_station->line, slot_index, departure_day);
    departure_slot->departure_rate = departure_rate(train_line_station, departure_day, slot_index);

}

/**
 * Copy departure details into the supplied departure_slot for the specified line, station, day, and departure slot
 */
void set_departure_slot_details_for_index(DepartureSlot *departure_slot, TrainLineStation *train_line_station, DayOfTheWeek departure_day, uint8_t slot_index) {

    trace("set_departure_slot_details_for_index", NULL);

    departure_slot->slot_index = slot_index;
    departure_slot->departure_minute = departure_minute(train_line_station, departure_day, slot_index);
    departure_slot->train_size = train_size(train_line_station->line, slot_index, departure_day);
    departure_slot->departure_rate = departure_rate(train_line_station, departure_day, slot_index);

}

/**
 * Copy departure details into the supplied departure_slot for the specified line, station, day and time
 */
void set_departure_slot_details_for_time(DepartureSlot *departure_slot, TrainLineStation *train_line_station, struct tm *for_time) {

    trace("set_departure_slot_details_for_time", NULL);

    uint8_t max_slot_index = number_of_departure_slots(train_line_station->line, for_time->tm_wday) - 1;

    uint16_t first_departure_in_minutes = departure_minute(train_line_station, for_time->tm_wday, 0);
    uint16_t last_departure_in_minutes = departure_minute(train_line_station, for_time->tm_wday, max_slot_index);
    uint16_t time_in_minutes = for_time->tm_hour * 60 + for_time->tm_min;

    // APP_LOG(APP_LOG_LEVEL_DEBUG, "time_in_minutes: %d, first_departure_in_minutes: %d, last_departure_in_minutes:%d", time_in_minutes, first_departure_in_minutes, last_departure_in_minutes);

    if (time_in_minutes < first_departure_in_minutes) {

        set_departure_slot_details_with_minutes(departure_slot, train_line_station, for_time->tm_wday, first_departure_in_minutes, 0);
        return;

    } else if (time_in_minutes > last_departure_in_minutes) {

        set_departure_slot_details_with_minutes(departure_slot, train_line_station, for_time->tm_wday, last_departure_in_minutes, max_slot_index);
        return;

    } else {

        uint8_t slot_index;
        uint16_t next_departure_minute = 0;
        //! todo - replace with binary search
        for (slot_index = 1; slot_index <= max_slot_index && next_departure_minute < time_in_minutes; slot_index++) {
            next_departure_minute = departure_minute(train_line_station, for_time->tm_wday, slot_index);
            // APP_LOG(APP_LOG_LEVEL_DEBUG, "time_in_minutes: %d, next_departure_minute:%d", time_in_minutes, next_departure_minute);
            if (time_in_minutes <= next_departure_minute) {
                set_departure_slot_details_with_minutes(departure_slot, train_line_station, for_time->tm_wday, next_departure_minute, slot_index);
                // APP_LOG(APP_LOG_LEVEL_DEBUG, "SELECT => time_in_minutes: %d, next_departure_minute:%d departure_slot:%d", time_in_minutes, next_departure_minute, departure_slot->slot_index);
                return;
            }
        }
        // APP_LOG(APP_LOG_LEVEL_ERROR, "ERROR: Departure slot was not set for -> time_in_minutes: %d, line:%s, station:%s", time_in_minutes, description_train_line(train_line_station->line), description_train_station(train_line_station->line, train_line_station->station));
        return;

    }

    APP_LOG(APP_LOG_LEVEL_ERROR, "ERROR: Departure slot was not set for -> time_in_minutes: %d, line:%s, station:%s", time_in_minutes, description_train_line(train_line_station->line), description_train_station(train_line_station->line, train_line_station->station));
}