/*
 * nexTTrain
 * ---------
 * 
 * Author:  Loubser Kotzé
 * Date:    8 August 2017
 */
#include <pebble.h>
#include "nexttrain.h"

#define LINE_SELECTION_CELL_HEIGHT 42
#define LINE_SELECTION_CELL_HEIGHT_ROUND 45

static Window *s_window;
static MenuLayer *s_menu_layer;

extern uint8_t number_of_stations(TrainLine train_line);
extern char *description_train_line(TrainLine train_line);
extern char *description_train_station(TrainLine train_line, uint8_t train_station);

static uint16_t get_num_rows_callback(MenuLayer *menu_layer, uint16_t section_index, void *context) {

    return number_of_stations(((WindowContext *) context)->temp_selected_line);

}

static void draw_row_callback(GContext *ctx, const Layer *cell_layer, MenuIndex *cell_index, void *callback_context) {

    static char title_buffer[10];
    static char subtitle_buffer[10];
    snprintf(title_buffer, sizeof (title_buffer), description_train_station(((WindowContext *) callback_context)->temp_selected_line, cell_index->row), (int) cell_index->row);
    snprintf(subtitle_buffer, sizeof (subtitle_buffer), description_train_line(((WindowContext *) callback_context)->temp_selected_line));
    menu_cell_basic_draw(ctx, cell_layer, title_buffer, subtitle_buffer, NULL);

}

static int16_t get_cell_height_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {

    //    if (is_round_pebble()) {
    //
    //        return menu_layer_is_index_selected(menu_layer, cell_index)
    //                ? MENU_CELL_ROUND_FOCUSED_SHORT_CELL_HEIGHT
    //                : MENU_CELL_ROUND_UNFOCUSED_TALL_CELL_HEIGHT;
    //
    //    } else {

    return PBL_IF_ROUND_ELSE(LINE_SELECTION_CELL_HEIGHT_ROUND, LINE_SELECTION_CELL_HEIGHT);

    //    }

}

static void select_callback(struct MenuLayer *menu_layer, MenuIndex *cell_index, void *callback_context) {

    WindowContext *context = (WindowContext *) callback_context;
    context->train_line_station->line = context->temp_selected_line;
    context->train_line_station->station = cell_index->row;
    APP_LOG(APP_LOG_LEVEL_DEBUG, "New station selected: %s", description_train_station(((WindowContext *) callback_context)->train_line_station->line, cell_index->row));
    window_stack_pop(true);
    window_stack_pop(true);

}

static void window_load_handler(Window *window) {

    Layer *window_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(window_layer);
    WindowContext *callback_context = (WindowContext *) window_get_user_data(window);

    s_menu_layer = menu_layer_create(GRect(bounds.origin.x, bounds.origin.y, bounds.size.w, bounds.size.h));
    menu_layer_set_click_config_onto_window(s_menu_layer, window);
    struct MenuLayerCallbacks menu_layers_callbacks = {
        .get_num_rows = get_num_rows_callback,
        .draw_row = draw_row_callback,
        .get_cell_height = get_cell_height_callback,
        .select_click = select_callback
    };
    menu_layer_set_callbacks(s_menu_layer, callback_context, menu_layers_callbacks);

    menu_layer_pad_bottom_enable(s_menu_layer, false);
    menu_layer_set_center_focused(s_menu_layer, false);
    menu_layer_set_highlight_colors(s_menu_layer, COLOR_FALLBACK(GColorBrass, GColorBlack), COLOR_FALLBACK(GColorBlack, GColorWhite));
    menu_layer_set_selected_index(s_menu_layer, (MenuIndex){0, callback_context->train_line_station->station}, MenuRowAlignCenter, true);

    layer_add_child(window_layer, menu_layer_get_layer(s_menu_layer));

}

static void window_unload_handler(Window *window) {

    menu_layer_destroy(s_menu_layer);
    window_destroy(window);
    s_window = NULL;

}

void train_station_selection_window_push(WindowContext *context) {

    if (!s_window) {
        s_window = window_create();
        window_set_user_data(s_window, context);
        struct WindowHandlers windows_handlers = {
            .load = window_load_handler,
            .unload = window_unload_handler
        };
        window_set_window_handlers(s_window, windows_handlers);
    }
    window_stack_push(s_window, true);

}
