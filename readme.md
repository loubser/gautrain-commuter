# Gautrain Commuter

Author: Loubser Kotze Date: 1 August 2017

## Overview
An app for the Pebble smart watch that counts down the departure of the next
train on a selected line and station.

## Features
### Implemented
* Encode the schedule, train size and fair bracket of all train departures for every line and station(North & South) 
* Select and recall a specific line and station
* Displays a count down of the next 2 trains departure time from the pre selected station
* On the main window, use the up and down buttons offset the schedule for the selected line and station
* Empty tracks icon for train already departed
* Encode the schedule, train size and fair bracket of all train departures for every line and station(East & West)
* Show departure time on each menu list
### Todo
* Quick view of next train departure in pebble menu
* Vibrate on train departure
* Train icons indicating North, South, East & West
* Direction icons for line selection and header display
* Building icons for train station selection
* Set alarm offset prior to a specified departure slot
* Indicator showing the last slot
* Long press the middle button to assign a morning or afternoon flag to the selected schedule (or remove it)
* First selection is either favorites or lines
* Automatically switch between morning and afternoon selection on opening the app
* Create a window to be able to see any days schedules
## Known bugs
* Countdown incorrect when departure time = current time (resolved)
* Sometimes menu list stops functioning correctly (usually in the afternoons) (resolved) 
* If current time after last departure time, list selection defaults to first item and not last (resolved)
* If app is opened exactly on a current departure time, correct list item is not selected  (resolved)
* East line last two departure slots of screen

## Versions
2017-08-01: Version 0.1 - Set up the initial project structure and framework.  
2017-08-06: Version 0.2 - First working prototype with my most often used trains schedules hard coded   
                        on the display.  
2017-08-17: Version 0.5 - Selection of train lines, stations and departure slots implemented. 
                        Persistence of context. Context enabled train icons.  
2017-10-08: Version 1.1.0 - Rename app to "Gautrain Commuter". Published first version of the app to the pebble app store.  
2017-10-16: Version 1.2.0 - Refine train info badges. Resolve bug where incorrect departure slot is selected if current time equals next departure time.